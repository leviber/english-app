import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';

const styles = theme => ({
  title: {
    marginBottom: 16,
    fontSize: 16,
    color: theme.palette.text.secondary,
  },
  word: {
    marginBottom: 16,    
  },
});

class WordCard extends React.Component {    
  constructor(props) {
    super(props);
    this.state = {translatedWord:props.translatedWord,
                  translatedWordField:'',
                  isTranslateWrong: false};  
  }

  handleTranslatedWordChange(e){
    var isTranslatedWordWrong = this.state.translatedWord != e.target.value;
      this.setState({isTranslateWrong: isTranslatedWordWrong});
  };

  render()  {
    const { classes } = this.props;
    return (
    <div>
      <Card className={classes.card}>
        <CardContent>
          <Typography className={classes.title}>
            Word number {this.props.wordsCount}
          </Typography>
          <Typography className={classes.word} 
                      type="headline" 
                      component="h2">
            {this.props.word}
          </Typography>
          <TextField error={this.state.isTranslateWrong}
                     id="traslatedWord" 
                     label="Translated word" 
                     onChange={e => this.handleTranslatedWordChange(e)}/>
        </CardContent>
      </Card>
    </div>
    );
  };
}

// WordCard.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

export default withStyles(styles)(WordCard);

