import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';

const styles = {
  root: {
    width: '100%',
  },
};

function AppMainBar(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography type="title" color="inherit">
            {props.header}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

AppMainBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppMainBar);