import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'typeface-roboto';
import AppMainBar from './AppMainBar';
import WordCard from './WordCard';
import Grid from 'material-ui/Grid';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AppMainBar header='Translate or die'/>
        <div style={{padding:20}}>
        <Grid container justify="center" spacing={40}>
          <Grid item xs={12}>
            <WordCard wordsCount='1' word='Hello' translatedWord='שלום' />
          </Grid>
        </Grid>
        </div>
      </div>
    );
  }
}

export default App;
